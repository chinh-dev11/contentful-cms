import heroImg  from '../assets/hero.svg'
import '../styles/Hero.css'

const Hero = () => {
    return (
        <section className="hero">
            <div className="hero-center">
                <div className="hero-title">
                    <h1>Contentful CMS</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat, assumenda. Incidunt quibusdam autem numquam voluptas. Ad tempore aperiam blanditiis iure maiores labore animi eligendi minus, cumque reiciendis voluptatum sed modi?</p>
                </div>
                <div className="img-container">
                    <img src={heroImg} alt="woman and the browser" className='img' />
                </div>
            </div>
        </section>
    )
}

export default Hero