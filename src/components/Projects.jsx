import ProjectCard from './ProjectCard'
import { useFetchProjects } from '../fetchProjects'
import '../styles/Projects.css'

const Projects = () => {
    const { loading, projects } = useFetchProjects()

    if (loading) {
        return (
            <section className="projects">
                <h2>Loading...</h2>
            </section>
        )
    }

    return (
        <section className="projects">
            <div className="title">
                <h2>Projects</h2>
                <div className="title-underline"></div>
            </div>
            <div className="projects-center">
                {projects.map(project => (
                    <ProjectCard key={project.id} {...project} id={project.id} />
                ))}
            </div>
        </section>
    )
}

export default Projects