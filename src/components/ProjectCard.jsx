const ProjectCard = ({ id, image, title, url }) => {
    return (
        <a href={url} key={id} target="_blank" rel="noreferrer" className="project">
            <img src={image.url} alt={image.title} className="img" />
            <h5>{title}</h5>
        </a>
    )
}

export default ProjectCard