import { useState, useEffect } from 'react'
import { createClient } from 'contentful'

const client = createClient({
    space: import.meta.env.VITE_CONTENTFUL_SPACE,
    // environment: import.meta.env.VITE_CONTENTFUL_ENVIRONMENT, // defaults to 'master' if not set
    accessToken: import.meta.env.VITE_CONTENTFUL_API_KEY,
})

// custom hook
export const useFetchProjects = () => {
    const [loading, setLoading] = useState(true)
    const [projects, setProjects] = useState([])

    const getData = async () => {
        try {
            const response = await client.getEntries()
            const projects = response.items.map(item => {
                const { image, title, url } = item.fields
                const project = {
                    id: item.sys.id,
                    image: {
                        url: image?.fields?.file?.url,
                        title: image?.fields?.title
                    },
                    title,
                    url
                }

                return project
            })

            setProjects(projects)
            setLoading(false)
        } catch (err) {
            console.error(err)
            setLoading(false)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return { loading, projects }
}